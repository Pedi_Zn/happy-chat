import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:happy_chat/Models/ChatMessage.dart';
import 'package:happy_chat/Views/SplashScreen.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Hive.initFlutter();
  Hive.registerAdapter<ChatMessage>(ChatMessageAdapter());
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Happy Chat',
      locale: const Locale('fa', 'FA'),
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: 'IranSans',
        primarySwatch: createMaterialColor(fromHex('#1E1E2C')),
        dividerColor: Color(0xFFEEEEEE),
        shadowColor: Color(0XFFE5E5E5),
        cardColor: Color(0x00FFFFFF),
        primaryColor: Colors.white,
        errorColor: Colors.red,
        brightness: Brightness.light,
        indicatorColor: fromHex('#1E1E2C'),
        accentColor: fromHex('#1E1E2C'),
        primaryColorDark: fromHex('#1E1E2C'),
        primaryColorLight: fromHex('#2D2D44'),
      ),
      home: SplashScreen(),
    );
  }

  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  static MaterialColor createMaterialColor(Color color) {
    List strengths = <double>[.05];
    final swatch = <int, Color>{};
    final int r = color.red, g = color.green, b = color.blue;

    for (int i = 1; i < 10; i++) {
      strengths.add(0.1 * i);
    }
    strengths.forEach((strength) {
      final double ds = 0.5 - strength;
      swatch[(strength * 1000).round()] = Color.fromRGBO(
        r + ((ds < 0 ? r : (255 - r)) * ds).round(),
        g + ((ds < 0 ? g : (255 - g)) * ds).round(),
        b + ((ds < 0 ? b : (255 - b)) * ds).round(),
        1,
      );
    });
    return MaterialColor(color.value, swatch);
  }

  static String replacePersianNumber(String input) {
    const english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    const farsi = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];

    for (int i = 0; i < english.length; i++) {
      input = input.replaceAll(english[i], farsi[i]);
    }
    return input;
    // return input.split('').reversed.join();
  }

  static String phoneNumberFormat(String input) {
    input = input.substring(0, 4) +
        " " +
        input.substring(4, 7) +
        " " +
        input.substring(7, 11);
    return replacePersianNumber(input);
  }
}
