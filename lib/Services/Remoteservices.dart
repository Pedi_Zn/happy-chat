import 'dart:convert';

import 'package:happy_chat/Models/ContactModel.dart';
import 'package:happy_chat/Models/OtpModel.dart';
import 'package:happy_chat/Models/RegisterModel.dart';
import 'package:http/http.dart' as http;

class Dataservices {
  static var client = http.Client();
  static var hostUrl = "https://factor.behtarino.com";

  static Future<RegisterDevice> registerDevice(body) async {
    var response = await client.post(
      Uri.parse(hostUrl + "/api/v1/users/phone_verification/"),
      body: body,
    );
    var data = utf8.decode(response.bodyBytes);
    return registerDeviceFromJson(data);
  }

  static Future<ObtainAuthToken> obtainAuthToken(body) async {
    var response = await client.post(
      Uri.parse(hostUrl + "/api/v1/token_sign/"),
      body: body,
    );
    var data = utf8.decode(response.bodyBytes);
    return obtainAuthTokenFromJson(data);
  }

  static Future<GetContacts> getContacts(token) async {
    var response = await client.get(
        Uri.parse(hostUrl + "/utils/challenge/contact_list/"),
        headers: {"Authorization": "Token " + token});
    var data = utf8.decode(response.bodyBytes);
    print(data);
    return getContactsFromJson(data);
  }
}
