import 'package:flutter/cupertino.dart';
import 'package:happy_chat/Models/ChatMessage.dart';
import 'package:hive/hive.dart';

enum MQTTAppConnectionState {
  connected,
  disconnected,
  connecting,
  error,
  subscribed
}

class MQTTAppState with ChangeNotifier {
  MQTTAppConnectionState _appConnectionState =
      MQTTAppConnectionState.disconnected;

  String _receivedText = "";
  String _historyText = "";

  void setReceivedText(String text) {
    print("0000000 : " + text);
    Box<ChatMessage> contactsBox = Hive.box<ChatMessage>("User1");
    final messageList = contactsBox.values.toList().cast<ChatMessage>();
    for (var i = 0; i < messageList.length; i++) {
      print(messageList[i].text);
    }
    _receivedText = text;
    _historyText = _historyText + '\n' + _receivedText;
    print("History ---------- >  " + _historyText);
    notifyListeners();
  }

  void setAppConnectionState(MQTTAppConnectionState state) {
    _appConnectionState = state;
    notifyListeners();
  }

  String get getReceivedText => _receivedText;
  String get getHistoryText => _historyText;
  List<String> get getHistoryList => _historyText.split('\n');
  MQTTAppConnectionState get getAppConnectionState => _appConnectionState;
}
