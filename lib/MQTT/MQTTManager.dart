import 'dart:convert';
import 'package:get/get.dart';
import 'package:happy_chat/Boxes.dart';
import 'package:happy_chat/Controllers/ContactController.dart';
import 'package:happy_chat/Controllers/OtpController.dart';
import 'package:happy_chat/MQTT/State/MQTTAppState.dart';
import 'package:happy_chat/Models/ChatMessage.dart';
import 'package:hive/hive.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqtt_client/mqtt_server_client.dart';

class MQTTManager {
  final OtpController otpController = Get.put(OtpController());
  final ContactController contactController = Get.put(ContactController());
  final String host = "185.86.181.206";
  final int port = 31789;

  late MqttServerClient client;
  late MQTTAppState currentState = MQTTAppState();
  late String otpToken;

  String userToken;
  int index;

  MQTTManager(this.userToken, this.index);

  configureAndConnect() async {
    otpToken = otpController.token;
    client = MqttServerClient.withPort(host, "#", port);
    client.logging(on: false);
    client.keepAlivePeriod = 20;
    client.onDisconnected = _onDisconnected;
    client.onConnected = _onConnected;
    client.onSubscribed = _onSubscribed;
    client.connectionMessage = MqttConnectMessage()
      ..authenticateAs("challenge", "8dAtPHvjPNC4erjFRfy");
    await connectClient();
    subscribeToTopic();
  }

  Future<void> connectClient() async {
    try {
      print("[MQTT] Client connecting ...");
      currentState.setAppConnectionState(MQTTAppConnectionState.connecting);
      await client.connect();
    } on Exception catch (e) {
      print("[MQTT] Client error => $e");
      currentState.setAppConnectionState(MQTTAppConnectionState.error);
      client.disconnect();
    }
  }

  void publishMessage(String message) {
    final MqttClientPayloadBuilder builder = MqttClientPayloadBuilder();
    builder.addUTF8String(message);
    print(
        "[MQTT] Publishing $message to topic challenge/user/$userToken/$otpToken/");
    client.publishMessage(
      "challenge/user/" + userToken + "/" + otpToken + "/",
      MqttQos.atLeastOnce,
      builder.payload!,
    );
  }

  void subscribeToTopic() {
    print("userToken  :" + userToken);
    print("otpToken  :" + otpToken);
    print("[MQTT] Subscribing to topic challenge/user/$userToken/$otpToken/");
    client.subscribe("challenge/user/" + userToken + "/" + otpToken + "/",
        MqttQos.atLeastOnce);
    client.updates!.listen((List<MqttReceivedMessage<MqttMessage>> c) {
      final message = c[0].payload as MqttPublishMessage;
      String payload =
          MqttPublishPayload.bytesToStringAsString(message.payload.message);
      String decodeMessage = Utf8Decoder().convert(payload.codeUnits);
      print("[MQTT] Client Decoded Message $decodeMessage");

      var chatMessage = ChatMessage(
          id: 1, text: "User1:" + decodeMessage, date: DateTime.now());

      final box = Boxes.getChatMessages(index);
      box.add(chatMessage);
      currentState.setReceivedText("User1:" + decodeMessage);
    });
  }

  void _onSubscribed(String topic) {
    print('[MQTT] Subscription confirmed for $topic');
    currentState.setAppConnectionState(MQTTAppConnectionState.subscribed);
  }

  void _onDisconnected() {
    print("[MQTT] Client disconnected");
    currentState.setAppConnectionState(MQTTAppConnectionState.disconnected);
  }

  void _onConnected() {
    print("[MQTT] Client connected");
    currentState.setAppConnectionState(MQTTAppConnectionState.connected);
  }
}
