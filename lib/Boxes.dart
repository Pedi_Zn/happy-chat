import 'package:happy_chat/Models/ChatMessage.dart';
import 'package:hive/hive.dart';

class Boxes {
  static Box<ChatMessage> getChatMessages(int index) =>
      Hive.box<ChatMessage>("ChatNumber:" + index.toString());
}
