import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/get.dart';
import 'package:happy_chat/Controllers/OtpController.dart';
import 'package:happy_chat/Models/ChatMessage.dart';
import 'package:happy_chat/Models/ContactModel.dart';
import 'package:happy_chat/Services/Remoteservices.dart';
import 'package:hive/hive.dart';

class ContactController extends GetxController {
  final OtpController otpController = Get.put(OtpController());
  var isLoading = true.obs;
  var contactList = <Contacts>[].obs;

  @override
  void onInit() async {
    super.onInit();
    var box = await Hive.openBox('token');
    if (box.get("token") != "" && box.get("token") != null)
      contactInfo(box.get("token"));
    else
      contactInfo(otpController.token);
  }

  void contactInfo(token) async {
    print(token.toString());
    isLoading(true);
    try {
      var contactRes = await Dataservices.getContacts(token);
      if (contactRes.meta.statusCode == 200) {
        contactList.value = contactRes.data;
        print("-------------" + contactRes.toString());
        for (var i = 0; i < contactList.length; i++) {
          await Hive.openBox<ChatMessage>("ChatNumber:" + i.toString());
        }
      } else {}
    } finally {
      isLoading(false);
    }
  }
}
