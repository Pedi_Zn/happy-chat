import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/get.dart';
import 'package:happy_chat/Services/Remoteservices.dart';
import 'package:happy_chat/Views/OtpScreen.dart';

class RegisterController extends GetxController {
  var isLoading = false.obs;
  var disableButton = true.obs;
  var textError = false.obs;
  dynamic error = "".obs;
  var textEditingController = TextEditingController().obs;

  @override
  void onInit() {
    super.onInit();
    textEditingController.value.text = "";
  }

  @override
  void onClose() {
    super.onClose();
  }

  void checkHasError() {
    print(error);
    error == "" ? textError(false) : textError(true);
  }

  void checkTextLength() {
    textEditingController.value.text.length == 0
        ? disableButton(true) && textError(false)
        : disableButton(false);
  }

  void register(phoneNumber) async {
    isLoading(true);
    try {
      var registerRes =
          await Dataservices.registerDevice({"phone": phoneNumber});
      if (registerRes.meta.statusCode == 200) {
        error = "";
        Get.to(OtpScreen());
      } else {
        String json = registerRes.meta.detail;
        var json2 = json.replaceAll("'", "*");
        json2 = json2.replaceAll('"', "'");
        Map<String, dynamic> map = jsonDecode(json2.replaceAll('*', '"'));
        error = map['phone'];
        checkHasError();
        print(error);
      }
    } finally {
      isLoading(false);
    }
  }
}
