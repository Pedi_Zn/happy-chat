import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/get.dart';
import 'package:happy_chat/Views/ContactScreen.dart';
import 'package:happy_chat/Views/OtpScreen.dart';
import 'package:happy_chat/Views/RegisterScreen.dart';
import 'package:happy_chat/main.dart';
import 'package:hive/hive.dart';

class SplashController extends GetxController {
  var isLoading = true.obs;

  @override
  void onInit() async {
    super.onInit();
    initialApp();
  }

  void initialApp() async {
    await Future.delayed(Duration(seconds: 3));

    try {
      isLoading(true);
      var res = true; // Preload Requests
      if (res) {
        var box = await Hive.openBox('token');
        print("---------------------------" + box.get("token").toString());
        if (box.get("token") != "" && box.get("token") != null)
          Get.to(ContactScreen());
        else
          Get.to(RegisterScreen());
      } else {
        Get.snackbar('', '',
            backgroundColor: Colors.transparent,
            snackPosition: SnackPosition.BOTTOM,
            titleText: Container(),
            duration: Duration(milliseconds: 2300),
            messageText: Center(
              child: Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: MyApp.fromHex('#2D2D44')),
                child: Center(
                  child: Text(
                    "خطا در ارتباط با سرور",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ),
            margin: const EdgeInsets.all(10));
      }
    } catch (e) {
      print(e.toString());
      isLoading(false);
    } finally {
      // isLoading(false);
    }
  }
}
