import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/get.dart';
import 'package:happy_chat/Services/Remoteservices.dart';
import 'package:happy_chat/Views/ContactScreen.dart';
import 'package:happy_chat/Views/OtpScreen.dart';
import 'package:hive/hive.dart';

class OtpController extends GetxController {
  var token = "";
  var isLoading = false.obs;
  var textError = false.obs;
  var resend = true.obs;
  var otpDigitOne = TextEditingController().obs;
  var otpDigitTwo = TextEditingController().obs;
  var otpDigitThree = TextEditingController().obs;
  var otpDigitFour = TextEditingController().obs;

  var counter = 30.obs;
  late Timer timer;

  @override
  void onInit() {
    super.onInit();
    startTimer();
    otpDigitOne.value.text = "";
    otpDigitTwo.value.text = "";
    otpDigitThree.value.text = "";
    otpDigitFour.value.text = "";
  }

  @override
  void onClose() {
    super.onClose();
  }

  bool checkTimerEnd() {
    if (counter.value == 0)
      return true;
    else
      return false;
  }

  void startTimer() {
    textError(false);
    isLoading(false);
    counter = 30.obs;
    timer = Timer.periodic(
      Duration(seconds: 1),
      (timer) {
        if (counter > 0) {
          resend(true);
          counter--;
        } else {
          resend(false);
          timer.cancel();
        }
      },
    );
  }

  String mergeControllerText() {
    return otpDigitOne.value.text +
        otpDigitTwo.value.text +
        otpDigitThree.value.text +
        otpDigitFour.value.text;
  }

  void otp(phoneNumber) async {
    print(phoneNumber);
    isLoading(true);
    try {
      var otpRes = await Dataservices.obtainAuthToken(
          {"username": phoneNumber, "password": mergeControllerText()});
      if (otpRes.meta.statusCode == 200) {
        textError(false);
        token = otpRes.data.token;
        var box = await Hive.openBox('token');
        box.put("token", token);
        Get.to(ContactScreen());
      } else {
        textError(true);
      }
    } finally {
      isLoading(false);
    }
  }
}
