import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:happy_chat/MQTT/MQTTManager.dart';
import 'package:happy_chat/Models/ChatMessage.dart';
import 'package:happy_chat/Models/ContactModel.dart';
import 'package:happy_chat/boxes.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:hive/hive.dart';

class ChatScreen extends StatefulWidget {
  final Contacts contacts;
  final int index;

  const ChatScreen({Key? key, required this.contacts, required this.index})
      : super(key: key);

  @override
  ChatScreenState createState() => ChatScreenState();
}

class ChatScreenState extends State<ChatScreen> {
  TextEditingController controller = TextEditingController();
  late MQTTManager mqttManager;

  @override
  void initState() {
    super.initState();
    mqttManager = MQTTManager(widget.contacts.token.toString(), widget.index);
    mqttManager.configureAndConnect();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _buildAppbar(),
            _buildScrollableTextWith(widget.index),
            _buildMessageTextField(),
          ],
        ),
      ),
    );
  }

  Widget _buildAppbar() {
    return Container(
      height: 60.0,
      padding: EdgeInsets.all(10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: Icon(
                  Icons.arrow_back,
                  size: 26,
                ),
              ),
              Text(
                "بازگشت",
                style: TextStyle(
                    fontSize: 16.0, color: Theme.of(context).primaryColorDark),
              )
            ],
          ),
          Text(
            widget.contacts.name.toString(),
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Theme.of(context).primaryColorDark,
              fontSize: 15,
            ),
          ),
          CircleAvatar(
            radius: 25.0,
            backgroundImage: NetworkImage(
                "https://i.pravatar.cc/150?img=" + widget.index.toString()),
            backgroundColor: Colors.transparent,
          ),
        ],
      ),
    );
  }

  Widget _buildScrollableTextWith(int index) {
    return ValueListenableBuilder<Box<ChatMessage>>(
      valueListenable: Boxes.getChatMessages(index).listenable(),
      builder: (context, box, _) {
        final messageList = box.values.toList().cast<ChatMessage>();
        if (box.values.isEmpty) {
          return Expanded(
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image(
                    image: AssetImage(
                      "assets/images/portal.png",
                    ),
                    height: 200,
                  ),
                  SizedBox(height: 10.0),
                  Text(
                    "هنوز به این دنیا وارد نشدی.",
                    textDirection: TextDirection.rtl,
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    "یه پرتال بزن به گوشی رفیقت.",
                    textDirection: TextDirection.rtl,
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
          );
        }
        return Expanded(
          child: ListView.builder(
            padding: EdgeInsets.all(8),
            itemCount: messageList.length,
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                onLongPress: () async {
                  await box.deleteAt(index);
                },
                child: messageList[index].text.contains("User1:")
                    ? Wrap(
                        alignment: WrapAlignment.end,
                        children: [
                          Card(
                            color: Colors.red.shade200,
                            child: Container(
                              padding:
                                  EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
                              child: Text(
                                messageList[index].text.split("User1:")[1],
                                style: TextStyle(
                                    color: Colors.black, fontSize: 20.0),
                              ),
                            ),
                          ),
                        ],
                      )
                    : Wrap(
                        alignment: WrapAlignment.start,
                        children: [
                          Card(
                            color: Colors.orange.shade200,
                            child: Container(
                              padding:
                                  EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
                              child: Text(
                                messageList[index].text,
                                style: TextStyle(
                                    color: Colors.black, fontSize: 20.0),
                              ),
                            ),
                          ),
                        ],
                      ),
              );
            },
          ),
        );
      },
    );
  }

  _buildMessageTextField() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15.0),
      height: 60.0,
      color: Colors.red.shade100,
      child: Row(
        textDirection: TextDirection.rtl,
        children: <Widget>[
          Expanded(
            child: TextField(
              controller: controller,
              textDirection: TextDirection.rtl,
              textCapitalization: TextCapitalization.sentences,
              onChanged: (value) {},
              decoration: InputDecoration.collapsed(
                hintText: 'نوشتن پیام...',
                hintTextDirection: TextDirection.rtl,
                hintStyle: TextStyle(),
              ),
            ),
          ),
          IconButton(
            icon: Icon(Icons.send),
            iconSize: 25.0,
            color: Theme.of(context).primaryColorDark,
            onPressed: () {
              if (controller.text.isNotEmpty) {
                setState(() {
                  mqttManager.publishMessage(controller.text);
                  controller.text = '';
                });
              }
            },
          ),
        ],
      ),
    );
  }
}
