import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:happy_chat/Controllers/ContactController.dart';
import 'package:happy_chat/Views/ChatScreen.dart';

class ContactScreen extends StatelessWidget {
  ContactScreen({Key? key}) : super(key: key);
  final ContactController contactController = Get.put(ContactController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: [
            Container(
              height: 120.0,
              padding: EdgeInsets.only(left: 25.0, right: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    onPressed: () {},
                    icon: Icon(
                      Icons.search,
                      size: 26,
                    ),
                  ),
                  CircleAvatar(
                    radius: 30.0,
                    backgroundImage:
                        NetworkImage("https://i.pravatar.cc/150?img=12"),
                    backgroundColor: Colors.transparent,
                  )
                ],
              ),
            ),
            Obx(() {
              return !contactController.isLoading.value
                  ? Container(
                      height: MediaQuery.of(context).size.height - 150,
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.only(left: 20.0, right: 20.0),
                      child: ListView.separated(
                        itemCount: contactController.contactList.length,
                        separatorBuilder: (context, index) {
                          return Divider();
                        },
                        itemBuilder: (context, index) {
                          return ListTile(
                            leading: CircleAvatar(
                              radius: 25.0,
                              backgroundImage: NetworkImage(
                                  "https://i.pravatar.cc/150?img=" +
                                      index.toString()),
                              backgroundColor: Colors.transparent,
                            ),
                            title: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  contactController.contactList[index].name,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16.0,
                                      color:
                                          Theme.of(context).primaryColorDark),
                                ),
                                Text(
                                  "هنوز پیامی ارسال نکرده‌اید...",
                                  style: TextStyle(
                                      fontSize: 14.0, color: Colors.grey),
                                ),
                              ],
                            ),
                            trailing: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  "هیچگاه",
                                  style: TextStyle(
                                      fontSize: 12.0, color: Colors.grey),
                                ),
                              ],
                            ),
                            onTap: () {
                              Get.to(ChatScreen(
                                  index: index,
                                  contacts:
                                      contactController.contactList[index]));
                            },
                          );
                        },
                      ),
                    )
                  : Center(child: CircularProgressIndicator());
            }),
          ],
        ),
      ),
    );
  }
}
