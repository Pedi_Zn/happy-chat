import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:happy_chat/Controllers/SplashController.dart';
import 'package:lottie/lottie.dart';

class SplashScreen extends StatelessWidget {
  final splashController = Get.put(SplashController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColorDark,
      body: Center(
        child: Column(
          children: [
            Spacer(),
            Container(
              child: Lottie.asset(
                "assets/images/splash.json",
                repeat: true,
              ),
            ),
            Spacer(),
            Obx(() {
              if (splashController.isLoading.value) {
                return CircularProgressIndicator(
                  strokeWidth: 2,
                  color: Theme.of(context).primaryColor,
                );
              } else
                return Container(
                  height: 35,
                  child: MaterialButton(
                    onPressed: () {
                      splashController.isLoading.value = true;
                      splashController.initialApp();
                    },
                    child: Text(
                      "تلاش مجدد",
                      style: TextStyle(color: Theme.of(context).primaryColor),
                    ),
                  ),
                );
            }),
            SizedBox(height: 20.0)
          ],
        ),
      ),
    );
  }
}
