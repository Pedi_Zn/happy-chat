import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:happy_chat/Controllers/RegisterController.dart';
import 'package:happy_chat/TextInputFormaters.dart';

class RegisterScreen extends StatelessWidget {
  final registerController = Get.put(RegisterController());
  RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Spacer(),
            Text(
              "هپی چت",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 48,
                  fontWeight: FontWeight.bold,
                  color: Theme.of(context).primaryColorLight),
            ),
            SizedBox(height: 70.0),
            Text(
              "برای ثبت‌نام شماره تلفن خود را وارد کنید.",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                color: Theme.of(context).primaryColorDark,
              ),
            ),
            SizedBox(height: 25),
            Obx(() {
              return Container(
                padding: EdgeInsets.only(left: 30, right: 30),
                child: new TextFormField(
                  inputFormatters: [
                    MaskedInputFormater('**** *** ****',
                        anyCharMatcher: RegExp(r'[0-9]'))
                  ],
                  controller: registerController.textEditingController.value,
                  onChanged: (s) async {
                    registerController.checkTextLength();
                    print(registerController.textEditingController.value.text);
                  },
                  cursorColor: Theme.of(context).primaryColorDark,
                  maxLength: 13,
                  textAlign: TextAlign.left,
                  autofocus: false,
                  keyboardType: TextInputType.number,
                  textDirection: TextDirection.ltr,
                  decoration: InputDecoration(
                    counterText: "",
                    contentPadding: EdgeInsets.symmetric(horizontal: -15.0),
                    suffixIcon: Container(
                      width: 90,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Container(
                            margin: EdgeInsets.all(8),
                            width: 1,
                            height: 40,
                            color: Colors.grey,
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(12, 0, 3, 0),
                            child: Image(
                              image: AssetImage(
                                "assets/images/iran.png",
                              ),
                              height: 40,
                            ),
                          ),
                        ],
                      ),
                    ),
                    hintText: "شماره تلفن خود را وارد نمایید",
                    hintTextDirection: TextDirection.rtl,
                    hintStyle: TextStyle(fontSize: 14),
                    focusedBorder: registerController.textError.value
                        ? const OutlineInputBorder(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(10.0)),
                            borderSide:
                                const BorderSide(color: Colors.red, width: 2.0),
                          )
                        : const OutlineInputBorder(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(10.0)),
                            borderSide: const BorderSide(
                                color: Colors.black, width: 2.0),
                          ),
                    border: OutlineInputBorder(
                        borderSide: const BorderSide(width: 2.0),
                        borderRadius: BorderRadius.circular(10.0)),
                  ),
                ),
              );
            }),
            SizedBox(height: 20.0),
            Obx(() {
              return registerController.textError.value
                  ? Text(
                      "به نظر می‌آید شماره تلفن معتبری وارد نکرده‌اید، مجدد تلاش کنید",
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).errorColor,
                      ),
                    )
                  : Container();
            }),
            Spacer(),
            Obx(() {
              return Container(
                padding: EdgeInsets.only(left: 30, right: 30),
                child: MaterialButton(
                  onPressed: !registerController.disableButton.value
                      ? () {
                          registerController.register(registerController
                              .textEditingController.value.text
                              .replaceAll(" ", ""));
                        }
                      : null,
                  disabledColor:
                      Theme.of(context).primaryColorDark.withOpacity(0.5),
                  color: Theme.of(context).primaryColorDark,
                  minWidth: MediaQuery.of(context).size.width,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: (registerController.isLoading.value)
                      ? CircularProgressIndicator(
                          strokeWidth: 2,
                          color: Theme.of(context).primaryColor,
                        )
                      : Text(
                          "ثبت‌نام",
                          style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 20),
                        ),
                  height: 65.0,
                ),
              );
            }),
            SizedBox(
              height: 30.0,
            ),
          ],
        ),
      ),
    );
  }
}
