import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:happy_chat/Controllers/OtpController.dart';
import 'package:happy_chat/Controllers/RegisterController.dart';

class OtpScreen extends StatelessWidget {
  OtpScreen({Key? key}) : super(key: key);

  final otpController = Get.put(OtpController());
  final registerController = Get.put(RegisterController());

  FocusNode _focusNodeDigitOne = FocusNode();
  FocusNode _focusNodeDigitTwo = FocusNode();
  FocusNode _focusNodeDigitThree = FocusNode();
  FocusNode _focusNodeDigitFour = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _body(context),
    );
  }

  Widget _body(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Colors.white, Colors.red.shade50])),
      child: Column(
        children: <Widget>[
          Container(
            height: 80.0,
            padding: EdgeInsets.only(top: 5.0, right: 10.0),
            child: Column(
              children: [
                SizedBox(height: 10.0),
                Row(
                  children: [
                    IconButton(
                      onPressed: () {
                        Get.back();
                      },
                      icon: Icon(
                        Icons.arrow_back,
                        size: 26,
                      ),
                    ),
                    Text(
                      "بازگشت",
                      style: TextStyle(
                          fontSize: 16.0,
                          color: Theme.of(context).primaryColorDark),
                    )
                  ],
                ),
                Spacer(),
              ],
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height - 80,
            padding: const EdgeInsets.all(40.0),
            child: Column(
              children: [
                Text(
                  "هپی چت",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 48,
                      fontWeight: FontWeight.bold,
                      color: Theme.of(context).primaryColorLight),
                ),
                SizedBox(height: 40.0),
                Text(
                  "برای ثبت‌نام کد ۴ رقمی ارسال شده را وارد کنید.",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Theme.of(context).primaryColorDark,
                  ),
                ),
                SizedBox(height: 20.0),
                Container(
                  child: _boxBuilder(context),
                ),
                SizedBox(height: 20.0),
                Obx(() {
                  return otpController.textError.value
                      ? Text(
                          "کد وارد‌شده معتبر نمی‌باشد، مجدد تلاش کنید",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: 13,
                            fontWeight: FontWeight.bold,
                            color: Theme.of(context).errorColor,
                          ),
                        )
                      : otpController.isLoading.value ||
                              registerController.isLoading.value
                          ? CircularProgressIndicator(
                              strokeWidth: 2,
                              color: Theme.of(context).primaryColorDark,
                            )
                          : Container();
                }),
                SizedBox(height: 20.0),
                Obx(() {
                  return otpController.resend.value
                      ? Text(
                          "ارسال مجدد تا " +
                              otpController.counter.value.toString() +
                              " ثانیه دیگر",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: Colors.grey,
                          ),
                        )
                      : GestureDetector(
                          onTap: () async {
                            registerController.register(registerController
                                .textEditingController.value.text
                                .replaceAll(" ", ""));
                            otpController.startTimer();
                          },
                          child: Text(
                            "ارسال مجدد کد",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).primaryColorLight,
                            ),
                          ),
                        );
                }),
                Spacer(),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _boxBuilder(BuildContext context) {
    return Obx(() {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            height: 70.0,
            width: 60.0,
            child: TextFormField(
              controller: otpController.otpDigitFour.value,
              focusNode: _focusNodeDigitFour,
              textDirection: TextDirection.ltr,
              onTap: () => otpController.otpDigitFour.value.selection =
                  TextSelection(
                      baseOffset: 0,
                      extentOffset:
                          otpController.otpDigitFour.value.value.text.length),
              onChanged: (s) {
                if (s != "")
                  FocusScope.of(context).unfocus();
                else
                  FocusScope.of(context).requestFocus(_focusNodeDigitThree);
                otpController.otp(registerController
                    .textEditingController.value.text
                    .replaceAll(" ", ""));
              },
              textAlign: TextAlign.center,
              keyboardType: TextInputType.number,
              maxLength: 1,
              decoration: InputDecoration(
                counterText: '',
                border: OutlineInputBorder(
                  borderSide: const BorderSide(width: 2.0, color: Colors.red),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                enabledBorder: otpController.textError.value
                    ? const OutlineInputBorder(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10.0)),
                        borderSide:
                            const BorderSide(color: Colors.red, width: 2.0),
                      )
                    : const OutlineInputBorder(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10.0)),
                        borderSide:
                            const BorderSide(color: Colors.grey, width: 2.0),
                      ),
              ),
            ),
          ),
          Container(
            alignment: Alignment.center,
            height: 70.0,
            width: 60.0,
            child: TextFormField(
              controller: otpController.otpDigitThree.value,
              focusNode: _focusNodeDigitThree,
              textDirection: TextDirection.ltr,
              onTap: () => otpController.otpDigitThree.value.selection =
                  TextSelection(
                      baseOffset: 0,
                      extentOffset:
                          otpController.otpDigitThree.value.value.text.length),
              onChanged: (s) {
                if (s != "")
                  FocusScope.of(context).requestFocus(_focusNodeDigitFour);
                else
                  FocusScope.of(context).requestFocus(_focusNodeDigitTwo);
              },
              textAlign: TextAlign.center,
              keyboardType: TextInputType.number,
              maxLength: 1,
              decoration: InputDecoration(
                counterText: '',
                border: OutlineInputBorder(
                  borderSide: const BorderSide(width: 2.0, color: Colors.red),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                enabledBorder: otpController.textError.value
                    ? const OutlineInputBorder(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10.0)),
                        borderSide:
                            const BorderSide(color: Colors.red, width: 2.0),
                      )
                    : const OutlineInputBorder(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10.0)),
                        borderSide:
                            const BorderSide(color: Colors.grey, width: 2.0),
                      ),
              ),
            ),
          ),
          Container(
            alignment: Alignment.center,
            height: 70.0,
            width: 60.0,
            child: TextFormField(
              controller: otpController.otpDigitTwo.value,
              focusNode: _focusNodeDigitTwo,
              textDirection: TextDirection.ltr,
              onTap: () => otpController.otpDigitTwo.value.selection =
                  TextSelection(
                      baseOffset: 0,
                      extentOffset:
                          otpController.otpDigitTwo.value.value.text.length),
              onChanged: (s) {
                if (s != "")
                  FocusScope.of(context).requestFocus(_focusNodeDigitThree);
                else
                  FocusScope.of(context).requestFocus(_focusNodeDigitOne);
              },
              textAlign: TextAlign.center,
              keyboardType: TextInputType.number,
              maxLength: 1,
              decoration: InputDecoration(
                counterText: '',
                border: OutlineInputBorder(
                  borderSide: const BorderSide(width: 2.0, color: Colors.red),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                enabledBorder: otpController.textError.value
                    ? const OutlineInputBorder(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10.0)),
                        borderSide:
                            const BorderSide(color: Colors.red, width: 2.0),
                      )
                    : const OutlineInputBorder(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10.0)),
                        borderSide:
                            const BorderSide(color: Colors.grey, width: 2.0),
                      ),
              ),
            ),
          ),
          Container(
            alignment: Alignment.center,
            height: 70.0,
            width: 60.0,
            child: TextFormField(
              controller: otpController.otpDigitOne.value,
              focusNode: _focusNodeDigitOne,
              textDirection: TextDirection.ltr,
              autofocus: true,
              onTap: () => otpController.otpDigitOne.value.selection =
                  TextSelection(
                      baseOffset: 0,
                      extentOffset:
                          otpController.otpDigitOne.value.value.text.length),
              onChanged: (s) {
                if (s != "")
                  FocusScope.of(context).requestFocus(_focusNodeDigitTwo);
                else
                  FocusScope.of(context).unfocus();
              },
              textAlign: TextAlign.center,
              keyboardType: TextInputType.number,
              maxLength: 1,
              decoration: InputDecoration(
                counterText: '',
                border: OutlineInputBorder(
                  borderSide: const BorderSide(width: 2.0, color: Colors.red),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                enabledBorder: otpController.textError.value
                    ? const OutlineInputBorder(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10.0)),
                        borderSide:
                            const BorderSide(color: Colors.red, width: 2.0),
                      )
                    : const OutlineInputBorder(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10.0)),
                        borderSide:
                            const BorderSide(color: Colors.grey, width: 2.0),
                      ),
              ),
            ),
          ),
        ],
      );
    });
  }

  Widget _box(BuildContext context, FocusNode focus) {
    return Container(
      alignment: Alignment.center,
      height: 70.0,
      width: 60.0,
      child: TextFormField(
        focusNode: focus,
        autofocus: true,
        textDirection: TextDirection.ltr,
        onChanged: (s) {
          if (s == "")
            FocusScope.of(context).previousFocus();
          else if (focus != _focusNodeDigitFour)
            FocusScope.of(context).nextFocus();
        },
        textAlign: TextAlign.center,
        keyboardType: TextInputType.number,
        maxLength: 1,
        decoration: InputDecoration(
          counterText: '',
          border: OutlineInputBorder(
              borderSide: const BorderSide(width: 2.0),
              borderRadius: BorderRadius.circular(10.0)),
          focusedBorder: otpController.textError.value
              ? const OutlineInputBorder(
                  borderRadius: const BorderRadius.all(Radius.circular(10.0)),
                  borderSide: const BorderSide(color: Colors.red, width: 2.0),
                )
              : const OutlineInputBorder(
                  borderRadius: const BorderRadius.all(Radius.circular(10.0)),
                  borderSide: const BorderSide(color: Colors.black, width: 2.0),
                ),
        ),
      ),
    );
  }
}
