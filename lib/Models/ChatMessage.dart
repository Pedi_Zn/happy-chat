import 'dart:convert';

import 'package:hive/hive.dart';
part 'ChatMessage.g.dart';

@HiveType(typeId: 1)
class ChatMessage extends HiveObject {
  @HiveField(0)
  int id;

  @HiveField(1)
  String text;

  @HiveField(2)
  DateTime date;

  ChatMessage({
    required this.id,
    required this.text,
    required this.date,
  });
}
