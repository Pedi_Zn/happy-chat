import 'dart:convert';

GetContacts getContactsFromJson(String str) =>
    GetContacts.fromJson(json.decode(str));

String getContactsToJson(GetContacts data) => json.encode(data.toJson());

class GetContacts {
  GetContacts({
    required this.meta,
    required this.data,
  });

  Meta meta;
  List<Contacts> data;

  factory GetContacts.fromJson(Map<String, dynamic> json) => GetContacts(
        meta: json["meta"] == null
            ? Meta.fromJson({})
            : Meta.fromJson(json["meta"]),
        data: json["data"] == null
            ? List<Contacts>.from({})
            : List<Contacts>.from(
                json["data"].map((x) => Contacts.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "meta": meta == null ? null : meta.toJson(),
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Contacts {
  Contacts({
    required this.id,
    required this.token,
    required this.name,
  });

  int id;
  String token;
  String name;

  factory Contacts.fromJson(Map<String, dynamic> json) => Contacts(
        id: json["id"] == null ? null : json["id"],
        token: json["token"] == null ? "" : json["token"],
        name: json["name"] == null ? "" : json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "token": token == null ? null : token,
        "name": name == null ? null : name,
      };
}

class Meta {
  Meta({
    required this.statusCode,
    required this.paginated,
    required this.detail,
  });

  int statusCode;
  bool paginated;
  String detail;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        statusCode: json["status_code"] == null ? null : json["status_code"],
        paginated: json["paginated"] == null ? false : json["paginated"],
        detail: json["detail"] == null ? "" : json["detail"],
      );

  Map<String, dynamic> toJson() => {
        "status_code": statusCode == null ? null : statusCode,
        "paginated": paginated == null ? null : paginated,
        "detail": detail == null ? null : detail,
      };
}
