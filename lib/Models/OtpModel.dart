import 'dart:convert';

ObtainAuthToken obtainAuthTokenFromJson(String str) =>
    ObtainAuthToken.fromJson(json.decode(str));

String obtainAuthTokenToJson(ObtainAuthToken data) =>
    json.encode(data.toJson());

class ObtainAuthToken {
  ObtainAuthToken({
    required this.meta,
    required this.data,
  });

  Meta meta;
  Data data;

  factory ObtainAuthToken.fromJson(Map<String, dynamic> json) =>
      ObtainAuthToken(
        meta: json["meta"] == null
            ? Meta.fromJson({})
            : Meta.fromJson(json["meta"]),
        data: json["data"] == null
            ? Data.fromJson({})
            : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "meta": meta == null ? null : meta.toJson(),
        "data": data == null ? null : data.toJson(),
      };
}

class Data {
  Data({
    required this.expiry,
    required this.token,
    required this.user,
  });

  String expiry;
  String token;
  User user;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        expiry: json["expiry"] == null ? "" : json["expiry"],
        token: json["token"] == null ? "" : json["token"],
        user: json["user"] == null
            ? User.fromJson({})
            : User.fromJson(json["user"]),
      );

  Map<String, dynamic> toJson() => {
        "expiry": expiry == null ? null : expiry,
        "token": token == null ? null : token,
        "user": user == null ? null : user.toJson(),
      };
}

class User {
  User({
    required this.username,
  });

  String username;

  factory User.fromJson(Map<String, dynamic> json) => User(
        username: json["username"] == null ? "" : json["username"],
      );

  Map<String, dynamic> toJson() => {
        "username": username == null ? null : username,
      };
}

class Meta {
  Meta({
    required this.statusCode,
    required this.paginated,
    required this.detail,
  });

  int statusCode;
  bool paginated;
  dynamic detail;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        statusCode: json["status_code"] == null ? null : json["status_code"],
        paginated: json["paginated"] == null ? false : json["paginated"],
        detail: json["detail"] == null ? "" : json["detail"],
      );

  Map<String, dynamic> toJson() => {
        "status_code": statusCode == null ? null : statusCode,
        "paginated": paginated == null ? null : paginated,
        "detail": detail == null ? null : detail,
      };
}
