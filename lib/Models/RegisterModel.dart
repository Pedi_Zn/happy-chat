import 'dart:convert';

RegisterDevice registerDeviceFromJson(String str) =>
    RegisterDevice.fromJson(json.decode(str));

String registerDeviceToJson(RegisterDevice data) => json.encode(data.toJson());

class RegisterDevice {
  RegisterDevice({
    required this.meta,
    required this.data,
  });

  Meta meta;
  Data data;

  factory RegisterDevice.fromJson(Map<String, dynamic> json) => RegisterDevice(
        meta: json["meta"] == null
            ? Meta.fromJson({})
            : Meta.fromJson(json["meta"]),
        data: json["data"] == null
            ? Data.fromJson({})
            : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "meta": meta == null ? null : meta.toJson(),
        "data": data == null ? null : data.toJson(),
      };
}

class Data {
  Data({
    required this.ok,
  });

  bool ok;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        ok: json["ok"] == null ? false : json["ok"],
      );

  Map<String, dynamic> toJson() => {
        "ok": ok == null ? "" : ok,
      };
}

class Meta {
  Meta({
    required this.statusCode,
    required this.paginated,
    required this.detail,
  });

  int statusCode;
  bool paginated;
  dynamic detail;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        statusCode: json["status_code"] == null ? null : json["status_code"],
        paginated: json["paginated"] == null ? false : json["paginated"],
        detail: json["detail"] == null ? "" : json["detail"],
      );

  Map<String, dynamic> toJson() => {
        "status_code": statusCode,
        "paginated": paginated,
        "detail": detail,
      };
}
